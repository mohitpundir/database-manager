from .common import *
from .job import Job


class Digger:
    """
    Class to retrieve the data from a schema or a parameteric study
    """
    
    def __init__(self, path, schema, exception_list=['schema_space']):
        self.path = path
        self.schema = schema
        self.exception_list = exception_list
        self.job = Job(path, schema, exception_list)

    def isExceptionFile(self, filename):
        is_exception = False
        for exceptions in self.exception_list:
            if exceptions in filename:
                is_exception = True
        return is_exception

    def digQuantity(self, quantity : str, varying_parameter : str,
                    fixed_parameters : dict):
        """
        Method to dig a field values for a varying parametric point in the
        parametric space. The varying parametric point is defined as a
        combination of a a varying parameter and a list of paramters
        that are fixed.

        :varying_parameter: Field or parameter name which is varying
        :fixed_parameter: A list of parameters that are fixed. Defined as a python dictionary.
        """
        quantities = {}
        path_schema = self.path + '/' + self.schema    

        for parameter_space in glob.glob(path_schema + '/' + '*.h5'):
            if self.isExceptionFile(parameter_space):
                continue

            with h5py.File(parameter_space, 'r') as f:
                parameter_exists = False
                job_id  = None
                nb_parameters_matched  = 0
                vary_param_value = None
                for fixed_param, fixed_val in fixed_parameters.items():
                    if f['/parameters'].attrs[fixed_param] != fixed_val:
                        parameter_exists = False
                    else:
                        parameter_exists = True
                        job_id = f['/attributes'].attrs['ID']
                        vary_param_value = f['/parameters'].attrs[varying_parameter]
                        nb_parameters_matched += 1

                if parameter_exists and nb_parameters_matched == len(fixed_parameters):
                    quantities[job_id] = {}
                    quantities[job_id][quantity] = self.job.digQuantity(quantity, job_id)
                    quantities[job_id][varying_parameter] = vary_param_value

        return quantities

    def plotParametericSpace(self, layout=(4, 4), figsize=(15, 10)):
        dataframe = self.showParametericSpace(shell_display=False)
        #dataframe = dataframe * 1 # to convert boolean string value to 1 or 0
        extras = {'marker' : 'o'}
        dataframe.plot(subplots=True, layout=layout, figsize=figsize, include_bool=True,
                       **extras)
        plt.show()


    def showParametericSpace(self, shell_display=False):
        schema_folder = self.path + '/' + self.schema

        param = {}
        for job_space in glob.glob(schema_folder + '/' + '*.h5'):
            if self.isExceptionFile(job_space):
                continue

            all_keys = []
            with h5py.File(job_space, 'r') as f:
                for key, item in f['/parameters'].attrs.items():
                    all_keys.append(key)

        unique_keys = np.unique(np.asarray(all_keys))

        for key in unique_keys:
            param[key] = []

            #param['Status'] = []
            #param['Time Stamp'] = []
            #param['ID'] = []

        for job_space in glob.glob(schema_folder + '/' + '*.h5'):
            if self.isExceptionFile(job_space):
                continue

            with h5py.File(job_space, 'r') as f:
                #param['Status'].append(f['/attributes'].attrs['Status'])
                #param['Time Stamp'].append(f['/attributes'].attrs['Time Stamp'])
                #param['ID'].append(f['/attributes'].attrs['ID'])

                for key in unique_keys:
                    if key in f['/parameters'].attrs.keys():
                        param[key].append(f['/parameters'].attrs[key])
                    else:
                        param[key].append(np.nan)

        df = pd.DataFrame(param)

        # shift column 'Name' to first position
        #first_column = df.pop('ID')
        # insert column using insert(position,column_name,
        # first_column) function
        #df.insert(0, 'ID', first_column)

        df_trans = df.T
        
        #df = df.sort_values('Time Stamp')

        if shell_display:
            print(f"{bcolors.PARAMETERSPACE}{bcolors.BOLD}Parametric Space{bcolors.ENDC}")
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df_trans))

        if not shell_display:
            return df
