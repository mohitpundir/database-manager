from setuptools import setup

setup(name='dbmanager',
      version='0.1',
      description='HDF5 based database managern',
      url='https://gitlab.com/mohitpundir/database-manager',
      author='Mohit Pundir',
      author_email='mpundir@ethz.cg',
      license='MIT',
      packages=['dbmanager'],
      install_requires=[
          'argparse',
          'h5py',
          'datetime',
          'pandas',
          'tabulate',
          'multimethod'
      ],
      zip_safe=False)
