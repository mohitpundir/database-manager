import time
from dbmanager import Manager
import argparse
import numpy as np

path = '.'
schema = 'trial'

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, default='old')
parser.add_argument('--flux', type=float, default=1e-3)
parser.add_argument('--porosity', type=float, default=0.4)
parser.add_argument('--nodes', type=int, default=5)
parser.add_argument('--time', type=float, default=1)
parser.add_argument('--dt', type=float, default=1e-1)
args = parser.parse_args()

outputs = {'x' : np.zeros((1, args.nodes)),
           'y' : 0}

nb_steps = 10

# define and initialize the manager 
manager = Manager(path, schema, uid='test')
manager.initialize(parameters=vars(args))
manager.registerQuantities(outputs)

# if simualtions produces additional outputs, can register
# its name and format in manager, at the finalize stage manager
# will count the number of particular outputs produced and will
# update the database

additionals = {'flux' :  'pvd', 'stress' : 'txt'}
manager.registerAdditionals(additionals)

# run your simulations and push quantities to manager
for i in range(nb_steps):
    stress =  np.ones((10, 2))
    outputs['x'] = np.ones((1, args.nodes))*i
    outputs['y'] = i
    manager.pushQuantity(outputs)

# get the location of database to save additional outputs in other formats
database_location = manager.getDatabaseLocation()
np.savetxt(database_location + '/stress-01.txt', stress)

# finalize the manager at the end of the simulation
manager.finalize()
