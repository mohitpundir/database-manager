from .common import *


class Runner:
    def __init__(self):
        self.param = {}
        
    def createParametricSpace(self):
        parameters = []
        for key, value in self.param.items():
            parameters.append(value)

        parametric_space = list(itertools.product(*parameters))
        return parametric_space


        
        



class Manager(object):
    """
    Class to manage the storage of data
    """
    def __init__(self, path, schema, uid, comm=MPI.COMM_WORLD):
        """
        Constructor method to initialize the manager class

        :path: path where data is to be stored
        :schema: name of the parametric study under which different jobs will be run
        :uid: unique identifier for a job or run in a parametric study
        :comm: Parallel communication module
        """

        self.path = path
        self.schema = schema
        self.uid = uid
        self.database = None
        self.filename = None
        self.step = 1
        self.comm = comm
        self.createSchema()
        self.has_additionals = False


    def finalize(self):
        """
        Method to finalize the data management, must be run at the end of the program
        """
        self.comm.barrier()
        
        if self.comm.Get_rank() == 0:
            self.finalizeAttributes()
            if self.has_additionals:
                self.finalizeAdditionals()

            
    def finalizeAttributes(self):
        path_schema = self.path + '/' + self.schema + '/'    
        parameter_space = path_schema +  self.uid + '.h5'
                   
        with  h5py.File(parameter_space, 'a') as h5file:           
            h5file['/attributes'].attrs['Status'] = 'Success'


    def finalizeAdditionals(self):
        path_schema = self.path + '/' + self.schema + '/'    
        parameter_space = path_schema +  self.uid + '.h5'
        
        with  h5py.File(parameter_space, 'a', libver='v110') as h5file:
            for key , file_format in  h5file['/additionals'].attrs.items():
                file_pattern = self.database + '/' +  key + '*.' + file_format
                file_list = glob.glob( file_pattern)
                h5file.create_dataset( '/additionals/' + key, data=file_list)

                
    def createSchema(self):
        if self.comm.Get_rank() == 0:
            schema_folder = self.path + '/' + self.schema
            if not os.path.exists(schema_folder):
                os.makedirs(schema_folder)
        self.comm.barrier()


    def initialize(self, parameters=None):
        """
        Method to initialize the parameter fields. The
        outputs is defined as a Python dictionary where the entry can
        be a scalar value or numpy array.
        
        :parameters: A Python dictionary with parameters as key and the parameter value as entry
        """
        
        if self.comm.Get_rank() == 0:
            self.registerParameters(parameters)
            self.registerGitAttributes()
        else:
            self.database = None

        self.database = self.comm.bcast(self.database, root=0)

        self.comm.barrier()

    def registerParameters(self, parameters):
        """"
        Internal method to register parameters and create database space
        """
        path_schema = self.path + '/' + self.schema + '/'    
        parameter_space = path_schema +  self.uid + '.h5'
        if os.path.exists(parameter_space):
            os.remove(parameter_space)

        with  h5py.File(parameter_space, 'w') as h5file:
            grp = h5file.create_group('/attributes')
            grp.attrs['Time Stamp'] = str(datetime.datetime.now().replace(second=0,
                                                                          microsecond=0))
            grp.attrs['Status'] = 'Running'
            grp.attrs['ID'] = self.uid
            grp.attrs['Processors'] = self.comm.Get_size()
            
            grp = h5file.create_group('/parameters')
            for key , val in  parameters.items():
                if val is None:
                    continue
                if type(val) is dict:
                    grp.attrs.update(val)
                    continue
                grp.attrs[key] = val
                    
        self.database = self.path + '/' + self.schema  + '/' + self.uid
        if not os.path.exists(self.database):
            os.makedirs(self.database)

    
    def registerQuantities(self, quantities=None):
        """
        Method to initialize the outputs fields. The
        outputs is defined as a Python dictionary where the entry can
        be a scalar value or numpy array.
 
        :outputs: A Python dicitionary with field name as key and values as entry
        """

        if not os.path.exists(self.database):
            os.makedirs(self.database)

        prank = self.comm.Get_rank()
        psize = self.comm.Get_size()
        
        if psize == 1:
            database_file = self.database + '/database.h5'
        else:
            database_file = self.database + '/database_rank-' + str(prank) + '.h5'

        if os.path.exists(database_file):
            os.remove(database_file)
        
        with  h5py.File(database_file, 'w') as h5file:        
                                          
            for key, item in quantities.items():
                if np.isscalar(item):
                    h5file.create_dataset(str(key), data=np.asarray([item]), 
                                              compression="gzip", chunks=True, maxshape=(None, ))
                else:
                    try:
                        length = item.shape[1]
                        h5file.create_dataset(str(key), data=item, 
                                              compression="gzip", chunks=True, maxshape=(None,  length))
                    except IndexError:
                        length = item.shape[0]
                        x = np.resize(item, (1, length))
                        h5file.create_dataset(str(key), data=x, 
                                              compression="gzip", chunks=True, maxshape=(None, length ))

    def registerGitAttributes(self):
        path_schema = self.path + '/' + self.schema + '/'
        database_space =  path_schema + self.uid
        

        process = subprocess.Popen(["git", "remote", "-v"],
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True)
        
        stdout, stderr = process.communicate()
        stdout_string = str(stdout)

        parameter_space = path_schema +  self.uid + '.h5'
        file = open(database_space + "/git-data.txt", "w")
        file.write("\n")
        file.write("----- REMOTE ------ \n")
        file.write(stdout_string)
        file.close()

        
        process = subprocess.Popen(["git", "branch", "-v"],
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True)
        
        stdout, stderr = process.communicate()
        stdout_string = str(stdout)

        parameter_space = path_schema +  self.uid + '.h5'
        file = open(database_space + "/git-data.txt", "a")
        file.write("\n")
        file.write("----- BRANCH ------ \n")
        file.write(stdout_string)
        file.close()
        
        process = subprocess.Popen(["git", "rev-parse", "HEAD"],
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True)
        
        stdout, stderr = process.communicate()
        stdout_string = str(stdout)

        parameter_space = path_schema +  self.uid + '.h5'
        file = open(database_space + "/git-data.txt", "a")
        file.write("\n")
        file.write("----- LAST COMMIT ------ \n")
        file.write(stdout_string)
        file.close()


        process = subprocess.Popen(["git", "status"],
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True)
        
        stdout, stderr = process.communicate()
        stdout_string = str(stdout)

        parameter_space = path_schema +  self.uid + '.h5'
        file = open(database_space + "/git-data.txt", "a")
        file.write("\n")
        file.write("----- STATUS ------ \n")
        file.write(stdout_string)
        file.close()

        process = subprocess.Popen(["git", "diff", "HEAD"],
                                   stdout=subprocess.PIPE, 
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True)
        
        stdout, stderr = process.communicate()
        stdout_string = str(stdout)

        parameter_space = path_schema +  self.uid + '.h5'
        file = open(database_space + "/git-data.txt", "a")
        file.write("----- DIFFERENCE ------ \n")
        file.write(stdout_string)
        file.close()

                   
                        
    def registerAdditionals(self, additionals):
        path_schema = self.path + '/' + self.schema + '/'    
        parameter_space = path_schema +  self.uid + '.h5'
        
        with  h5py.File(parameter_space, 'a') as h5file:
            grp = h5file.create_group('/additionals')
            for key , val in  additionals.items():
                grp.attrs[key] = val

        path_schema = self.path + '/' + self.schema + '/'
        self.database = path_schema + '/' + self.uid

        if not os.path.exists(self.database):
            os.makedirs(self.database)
        
        self.has_additionals = True

    def pushQuantity(self, quantities):
        prank = self.comm.Get_rank()
        psize = self.comm.Get_size()

        if psize == 1:
            database_file = self.database + '/database.h5'
        else:
            database_file = self.database + '/database_rank-' + str(prank) + '.h5'

        with  h5py.File(database_file, 'a') as h5file:
            #h5file.swmr_mode = True
            for key, item in quantities.items():
                dataset =  h5file[key]
                if np.isscalar(item):
                    dataset.resize( self.step, axis=0)
                    dataset[-1] = np.asarray([item])
                else:
                    try:
                        length = item.shape[1]
                        dataset.resize(  (self.step, length))
                        dataset[-1, :] = item
                    except IndexError:
                        length = item.shape[0]
                        x = np.resize(item, (1, length))
                        dataset.resize(  (self.step, length))
                        dataset[-1, :] = x

                dataset.flush()
        self.step += 1       
                
    def getJobID(self, parameters):
        path_schema = self.path + '/' + self.schema    
        for parameter_space in glob.glob(path_schema + '/' + '*.h5'):
            if 'schema_space' in  parameter_space:
                continue
            with h5py.File(parameter_space, 'r') as f:
                parameter_exists = False
                job_id  = None
                nb_parameters_matched  = 0
                for file_param in f['/parameters'].attrs.keys():
                    if file_param in parameters.keys():
                        if f['/parameters'].attrs[file_param] != parameters[file_param]:
                            parameter_exists = False
                        else:
                            parameter_exists = True
                            job_id = f['/attributes'].attrs['ID']
                            nb_parameters_matched += 1
                        
                if parameter_exists and nb_parameters_matched == len(parameters):
                    return job_id
                
                
    def getQuantity(self, quantity, parameters):
        job_id = self.getJobID(parameters)
        val = None
        database_file = self.path + '/' + self.schema + '/' + job_id + '/database.h5'
        temporary_file = self.path + '/' + self.schema + '/' + job_id + '/tmp.h5'

        shutil.copy2(database_file, temporary_file)
        
        f =  h5py.File(temporary_file, 'r', libver='v110', swmr=True)
        dataset = f[quantity]
        val  = list(dataset)

        f.close()
        os.remove(temporary_file)
        
        return val


    def getAdditionalSpace(self, parameters, shell_display=False):
        schema_folder = self.path + '/' + self.schema
        job_id = self.getJobID(parameters)
        parameter_file = schema_folder + '/' + job_id  + '.h5'
                
        param = {}

        with h5py.File(parameter_file, 'r') as f:
            print(f"{bcolors.PARAMETERSPACE}Additional Data Space{bcolors.ENDC}")
            grp = f.require_group('additionals')
            
            for key in grp.attrs.keys():
                param[key] = []
                param[key].append(grp.attrs[key])
                if key in grp.keys():
                    dset = grp[key]
                    param[key].append(dset.shape)
                
        df = pd.DataFrame(param)
        if shell_display:
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df))

        if not shell_display:
            return df

    
    def getDatabaseSpace(self, parameters, shell_display=False):
        job_id = self.getJobID(parameters)
        database_file = self.path + '/' + self.schema + '/' + job_id + '/database.h5'
        temporary_file = self.path + '/' + self.schema + '/' + job_id + '/tmp.h5'

        param = {}
        shutil.copy2(database_file, temporary_file)

        with h5py.File(temporary_file, 'r') as f:
            print(f"{bcolors.PARAMETERSPACE}Database Space{bcolors.ENDC}")
            for var in f.keys():
                param[var] = []
                param[var].append(f[var].dtype)
                param[var].append(f[var].shape)

        os.remove(temporary_file)

        df = pd.DataFrame(param)
        if shell_display:
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df))

        if not shell_display:
            return df


    def getParameterSpace(self, shell_display=False):
        schema_folder = self.path + '/' + self.schema
        
        param = {}
        still_running = []
        for job_space in glob.glob(schema_folder + '/' + '*.h5'):
            if 'schema_space' in job_space:
                continue
            with h5py.File(job_space, 'r') as f:
                for key, item in f['/parameters'].attrs.items():
                    if key in param.keys():
                        param[key].append(item)
                    else:
                        param[key] = []
                        param[key].append(item)

                for key, item in f['/attributes'].attrs.items():
                    if key in param.keys():
                        param[key].append(item)
                    else:
                        param[key] = []
                        param[key].append(item)
    
        df = pd.DataFrame(param)
        df = df.sort_values('Time Stamp')
        print(f"{bcolors.PARAMETERSPACE}Parameter Space{bcolors.ENDC}")

        if shell_display:
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df))

        if not shell_display:
            return df

    def getJobStatus(self):
        schema_folder = self.path + '/' + self.schema

        run_param = {}
        run_param['Run ID'] = []
        run_param['Status'] = []
        run_param['Time Stamp'] = []
        run_param['Job ID'] = []
        print(f"{bcolors.RUNID}Job Details{bcolors.ENDC}")

        for job_space in glob.glob(schema_folder + '/' + '*.h5'):
            if 'schema_space' in  job_space:
                continue
            with h5py.File(job_space, 'r') as f:
                status = f['/attributes'].attrs['Status']
                run_id , run_status = self.getStatus(f['/attributes'].attrs['ID'])

                if status == 'Running' and run_status == 'RUNNING':
                    status = 'Running'
                elif status == 'Success' and run_status == 'DONE':
                    status = 'Finished'
                elif status == 'Running' and run_status == 'DONE':
                    status = 'Failed'
                elif  run_status == 'PENDING':
                    status = 'Pending'

                    
                run_param['Run ID'].append(run_id)
                run_param['Status'].append(status)
                run_param['Time Stamp'].append(f['/attributes'].attrs['Time Stamp'])
                run_param['Job ID'].append(f['/attributes'].attrs['ID'])
        
        df = pd.DataFrame(run_param)
        df = df.sort_values('Time Stamp')
        return df


    def getStatus(self, job_id):
        schema_space =  self.path + '/' + self.schema + '/schema_space.h5'
        status = None
        with h5py.File(schema_space, 'r') as sch:
            run_id = sch[str(job_id)].attrs['run']
            process = subprocess.Popen(["bjobs", str(run_id)],
                                       stdout=subprocess.PIPE, 
                                       stderr=subprocess.PIPE,
                                       universal_newlines=True)
            
            stdout, stderr = process.communicate()
            stdout_string = str(stdout)
            if 'PEND' in stdout.split(' '):
                status = 'PENDING'
            elif 'RUN' in stdout.split(' '):
                status = 'RUNNING'
            else:
                status = 'DONE'
                
        return run_id, status


    def getDatabaseLocation(self, parameters=None):
        if not parameters is None:
            job_id = self.getJobID(parameters)
            database_location = self.path + '/' + self.schema + '/' + job_id
            return database_location
        else:
            database_location = self.path + '/' + self.schema + '/' + self.uid
            return database_location


    ################################

    def getGroupPath(self, args):
        group_path = '/'
        for key, value in vars(args).items():
            if key == 'idx':
                continue
            group_path +=  str(value) + '-'
        group_path = group_path[:-1]
        return group_path

    def createTemporaryFile(self, filename):
        tmp_file = self.path + '/' + self.schema + '/tmp.h5'
        shutil.copy2(filename, tmp_file)
        return tmp_file

        
    def createDatabase(self, args, data_dict):
        self.createSchema()
        path_schema = self.path + '/' + self.schema + '/'    
       
        group_path = self.getGroupPath(args)
        time_stamp = str(datetime.datetime.now())

        self.filename  = path_schema
        for key, value in vars(args).items():
            if key == 'idx':
                continue
            self.filename +=  str(value) + '-'
            
        self.filename = self.filename[:-1] + '.h5'

        with  h5py.File(self.filename, 'w') as h5file:        
            grp = h5file.create_group(group_path)
            grp.attrs['Time Stamp'] = str(datetime.datetime.now().replace(second=0, microsecond=0))
            grp.attrs['ID'] = args.idx
            grp.attrs['Status'] = 'Running'

            for key , val in vars(args).items():
                if key == 'idx':
                    continue
                grp.attrs[key] = val
                                   
            for key, item in data_dict.items():
                if np.isscalar(item):
                    h5file.create_dataset(group_path + '/' + str(key), data=np.asarray([item]), 
                                              compression="gzip", chunks=True, maxshape=(None, ))
                else:
                    try:
                        length = item.shape[1]
                        h5file.create_dataset(group_path + '/' + str(key), data=item, 
                                              compression="gzip", chunks=True, maxshape=(None,  length))
                    except IndexError:
                        length = item.shape[0]
                        x = np.resize(item, (1, length))
                        h5file.create_dataset(group_path + '/' + str(key), data=x, 
                                              compression="gzip", chunks=True, maxshape=(None, length ))

    
    def saveToDatabase(self, args, data_dict):
        self.step += 1
        group_path = self.getGroupPath(args)
                
        with  h5py.File(self.filename, 'a') as h5file:           
            for key, item in data_dict.items():
                dataset =  h5file[group_path][key]
                if np.isscalar(item):
                    dataset.resize( self.step, axis=0)
                    dataset[-1] = np.asarray([item])
                else:
                    try:
                        length = item.shape[1]
                        dataset.resize(  (self.step, length))
                        dataset[-1, :] = item
                    except IndexError:
                        length = item.shape[0]
                        x = np.resize(item, (1, length))
                        dataset.resize(  (self.step, length))
                        dataset[-1, :] = x



    def getGroup(self, parameters):
        schema_folder = self.path + '/' + self.schema
                
        for run_space in glob.glob(schema_folder + '/' + '*.h5'):
            if 'schema_space' in run_space or 'tmp.h5' in run_space:
                continue
            tmp_space = self.createTemporaryFile(run_space)
            with h5py.File(tmp_space, 'r') as f:     
                for grp in f.keys():
                    parameter_exists = False
                    group_name  = None
                    nb_parameters_matched  = 0
                    for param in parameters.keys():
                        if param == 'run' or param =='Time Stamp' or param == 'Status' or param=='ID':
                            nb_parameters_matched += 1
                            continue
                        if f[grp].attrs[param] != parameters[param]:
                            parameter_exists = False
                        else:
                            parameter_exists = True
                            group_name = grp
                            nb_parameters_matched += 1
                    if parameter_exists and nb_parameters_matched == len(parameters):
                        return group_name
            os.remove(tmp_space)
            
        raise RuntimeError('Parameter space does not exists')

    
    def getData(self, name, parameters):
        group_name = self.getGroup(parameters)
        val = None
        parameter_file = group_name
        parameter_file = self.path + '/' + self.schema + '/' + parameter_file + '.h5'

        tmp_space = self.createTemporaryFile(parameter_file)
        with h5py.File(tmp_space, 'r') as f:
            val = list(f[group_name][name])

        os.remove(tmp_space)
        return val

    def getRunStatus(self, idx):
        schema_space =  self.path + '/' + self.schema + '/schema_space.h5'
        status = None
        with h5py.File(schema_space, 'r') as sch:
            run_id = sch[str(idx)].attrs['run']
            process = subprocess.Popen(["bjobs", str(run_id)],
                                       stdout=subprocess.PIPE, 
                                       stderr=subprocess.PIPE,
                                       universal_newlines=True)
            
            stdout, stderr = process.communicate()
            stdout_string = str(stdout)
            if 'PEND' in stdout.split(' '):
                status = 'PENDING'
            elif 'DONE' in stdout.split(' '):
                status = 'DONE'
            elif 'RUN' in stdout.split(' '):
                status = 'RUNNING'
                        
        return run_id, status
    
    def getSchemaSpace(self):
        schema_folder = self.path + '/' + self.schema

        run_param = {}
        run_param['Run ID'] = []
        run_param['Status'] = []
        run_param['Time Stamp'] = []
        run_param['Job ID'] = []
        print(f"{bcolors.RUNID}Run Details{bcolors.ENDC}")

        for run_space in glob.glob(schema_folder + '/' + '*.h5'):
            if 'schema_space' in  run_space or 'tmp.h5' in run_space:
                continue
            tmp_space = self.createTemporaryFile(run_space)
            with h5py.File(tmp_space, 'r') as f:
                for grp in f.keys():
                    status = f[grp].attrs['Status']
                    run_id , run_status = self.getRunStatus(f[grp].attrs['ID'])

                    if status == 'Running' and run_status == 'RUNNING':
                        status = 'Running'
                    elif status == 'Running' and run_status == 'DONE':
                        status = 'Running'
                    
                    run_param['Run ID'].append(run_id)
                    run_param['Status'].append(status)
                    run_param['Time Stamp'].append(f[grp].attrs['Time Stamp'])
                    run_param['Job ID'].append(f[grp].attrs['ID'])

            os.remove(tmp_space)
        
        df = pd.DataFrame(run_param)
        df = df.sort_values('Time Stamp')
        return df
        
    def getParametricSpace(self, shell_display=False):
        schema_folder = self.path + '/' + self.schema
        
        param = {}
        still_running = []
        for run_space in glob.glob(schema_folder + '/' + '*.h5'):
            if 'schema_space' in run_space or 'tmp.h5' in run_space:
                continue
            tmp_space = self.createTemporaryFile(run_space)
            #try:
            with h5py.File(tmp_space, 'r') as f:
                for grp in f.keys():
                    for key, item in f[grp].attrs.items():
                        if key in param.keys():
                            param[key].append(item)
                        else:
                            param[key] = []
                            param[key].append(item)
            #except OSError:
            #    print(run_space)
            #    still_running.append(run_space)

            os.remove(tmp_space)

        df = pd.DataFrame(param)
        df = df.sort_values('Time Stamp')
        print(f"{bcolors.RUNID}Parameter Space{bcolors.ENDC}")

        if shell_display:
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df))

        if not shell_display:
            return df
                        
    def getVariableSpace(self, parameters):
        group_name = self.getGroup(parameters)
        parameter_file = self.path + '/' + self.schema + '/' + group_name + '.h5'
        tmp_space = self.createTemporaryFile(parameter_file)
        with h5py.File(tmp_space, 'r') as f:
            print(f"{bcolors.PARAMETERSPACE}Variable Space{bcolors.ENDC}")
            for var in f[group_name].keys():
                print('{message: <20}'.format(message=var),
                      '{type:}'.format(type=f[group_name][var].dtype), '{shape:}'.format(shape=f[group_name][var].shape))

        os.remove(tmp_space)


def test():
    path = '.'
    schema = 'trial'
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, default='old')
    parser.add_argument('--flux', type=float, default=1e-2)
    parser.add_argument('--porosity', type=float, default=0.5)
    parser.add_argument('--nodes', type=int, default=4)
    parser.add_argument('--time', type=float, default=1)
    parser.add_argument('--dt', type=float, default=1e-1)
    args = parser.parse_args()

    quantities = {'x' : np.zeros((1, args.nodes)),
                  'y' : 0}
       
    manager = Manager(path, schema, uid='test')
    '''manager.createSchema()
    manager.registerParameters(vars(args))
    manager.registerQuantities(quantities, 1)
    
    quantities['x'] = np.ones((1, args.nodes))
    quantities['y'] = 1
    manager.pushQuantity(quantities)'''

    param_dict = { 'model' : 'old',
                   'dt' : 1e-1,
                   'time' : 1.0,
                   'nodes' : 4,
                   'flux' : 1e-2,
                   'porosity': 0.5}
    
    val = manager.getQuantity('x', param_dict)
    print(val)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--test', action='store_false', default=False)
    parser.add_argument('--schema', type=str, default='trial')
    parser.add_argument('--path', type=str, default='.')
    args = parser.parse_args()

    test()
    
