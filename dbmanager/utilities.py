import argparse
import itertools



def is_float(string):
    if string.replace(".", "").isnumeric():
        return True

    return False

def is_int(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def get_type(x):
    if is_float(x):
        return float(x)
    if is_int(x):
        return int(x)

    return x


class Utility(argparse.Action):
    """
    Class which  giving functionalities to parsing data
    """
    def __call__ (self, parser, namespace, values, option_string = None):
        with values as f:
            # parse arguments in the file and store them in the target namespace
            parser.parse_args(f.read().split(), namespace)

    @staticmethod
    def str_to_dict(x):
        return {k:get_type(v) for k,v in (i.split(':') for i in x.split(','))}

    @staticmethod
    def str_to_bool(v):
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected')


    @staticmethod
    def create_parametric_space(param):
        parameters = []
        for key, value in param.items():
            parameters.append(value)

        parametric_space = list(itertools.product(*parameters))
        return parametric_space
    
