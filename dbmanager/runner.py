import subprocess
import uuid
import itertools
import h5py
from manager import Runner



path = '/cluster/work/cmbm/mpundir/homogenization_study/output/'
schema = 'micro-cell'

runner = Runner()
runner.param['chemical'] = ['old']
runner.param['distribution'] = ['isotropic']
runner.param['time'] = [100]
runner.param['length'] = [1, 10]
runner.param['nb_pores'] = [1]
runner.param['flux'] = [1]
runner.param['pH'] = [8]
runner.param['dt'] = [1]

command_file = "python main.py"
wall_time = "-W 12:00"
memory = "-R  rusage[mem=8192]"

for param in runner.createParametricSpace():
    chem, dist, time, length, pore, flux, pH, dt = param
 
    idx = uuid.uuid4().hex
    job_id = "-J " + str(idx)
    
    process = subprocess.Popen(["bsub", job_id, memory, wall_time,  command_file,
                                "--chemical", chem, "--distribution", dist, 
                                "--time", str(time), "--length", str(length),
                                "--nb_pores", str(pore), "--flux", str(flux), "--pH", str(pH),
                                "--dt", str(dt),
                                "--idx", str(idx)],
                               stdout=subprocess.PIPE, 
                               stderr=subprocess.PIPE,
                               universal_newlines=True)

    stdout, stderr = process.communicate()
    stdout_string = str(stdout)

    stdout_string = stdout_string.split(' ')
    run_id = stdout_string[1].split('<')
    run_id = run_id[1].split('>')
    run_id = run_id[0]

    schema_file = 'schema_space.h5'
    filename = path + '/'  + schema + '/' + schema_file
    
    with  h5py.File(filename, 'a') as h5file:
        encoded_run = run_id.encode("ascii", "ignore")
        grp = h5file.create_group(str(idx))
        grp.attrs['run'] = run_id
