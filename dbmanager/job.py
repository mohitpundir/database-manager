from .common import *


class Job(object):
    def __init__(self, path, schema, exception_list=['schema_space']):
        self.path = path
        self.schema = schema
        self.exception_list = exception_list
        self.mpi = False
        self.nb_processors = 1
        self.steps_to_retrive = []

    def isExceptionFile(self, filename):
        is_exception = False
        for exceptions in self.exception_list:
            if exceptions in filename:
                is_exception = True
        return is_exception

    def digID(self, parameters):
        """
        Method to get the unique identifier for a given list of parameters

        :parameters: A Python dicitonary with parameters and their values
        """
        
        path_schema = self.path + '/' + self.schema    
        job_ids_found = []
        for parameter_space in glob.glob(path_schema + '/' + '*.h5'):
            if self.isExceptionFile(parameter_space):
                continue

            with h5py.File(parameter_space, 'r') as f:
                self.processors = f['/attributes'].attrs['Processors']
                if self.processors > 1:
                    self.mpi = True
                    
                parameter_exists = False
                job_id  = None
                nb_parameters_matched  = 0
                for file_param in f['/parameters'].attrs.keys():
                    if file_param in parameters.keys():
                        if f['/parameters'].attrs[file_param] != parameters[file_param]:
                            parameter_exists = False
                        else:
                            parameter_exists = True
                            job_id = f['/attributes'].attrs['ID']
                            nb_parameters_matched += 1

                if parameter_exists and nb_parameters_matched == len(parameters):
                    job_ids_found.append(job_id)

        if len(job_ids_found) == 0:
            raise RuntimeWarning('Could not find a job for the parameter space requested')
        else:
            return job_ids_found


    def digQuantitiesForParameters(self, quantity_list : list,  parameters : dict):
        """
        Method to get a list of output fields for a given list of
        parameters (parametric point)

        :quantity_list: A list containing the names of the output fields
        :parameters: A dicitionary containing the parameters and their values
        """
        
        job_ids_found = self.digID(parameters)
        if len(job_ids_found) > 1:
            print('Job ID | ', job_ids_found)
            raise RuntimeError('Many jobs exists, choose one')

        job_id = job_ids_found[0]
        return self.digQuantitiesForJob(quantity_list, job_id)

    def digQuantitiesForJob(self, quantity_list : list,  job_id : str, copy=True):
        """
        Method to get a list of output fields for a ob id

        :quantity_list: A list containing the names of the output fields
        :job_id: A string unique id associated to a job
        """

        if copy is True:
            if self.mpi is False:
                database_file = self.path + '/' + self.schema + '/' + job_id + '/database.h5'
                temporary_file = self.path + '/' + self.schema + '/' + job_id + '/tmp.h5'
                shutil.copy2(database_file, temporary_file)

            elif self.mpi is True:
                for prank in range(self.processors):
                    database_file = self.path + '/' + self.schema + '/' + job_id + '/database_rank-' + str(prank) + '.h5'
                    temporary_file = self.path + '/' + self.schema + '/' + job_id + '/tmp_rank-' + str(prank) + '.h5'
                    shutil.copy2(database_file, temporary_file)

        quantities = {}

        for quantity in quantity_list:
            quantities[quantity] = self._digQuantityForJob(quantity, job_id, copy)

        if copy is True:
            if self.mpi is False:
                temporary_file = self.path + '/' + self.schema + '/' + job_id + '/tmp.h5'
                os.remove(temporary_file)

            elif self.mpi is True:
                for prank in range(self.processors):
                    temporary_file = self.path + '/' + self.schema + '/' + job_id + '/tmp_rank-' + str(prank) + '.h5'
                    os.remove(temporary_file)
                
        return quantities

    def _digQuantityForParameters(self, quantity : str, parameters : dict, copy=True):
        """
        Method to get a output field for a given list of
        parameters (parametric point)

        :quantity: Name of the output field
        :parameters: A dicitionary containing the parameters and their values
        """
        
        job_ids_found = self.digID(parameters)
        if len(job_ids_found) > 1:
            print('Job ID | ', job_ids_found)
            raise RuntimeError('Many jobs exists, choose one')

        job_id = job_ids_found[0]
        
        val = self._digQuantityForJob(quantity, job_id, copy)      
        return val

    def _digQuantityForJob(self, quantity : str, job_id : str,  copy : bool):
        """
        Method to get a output field for a job id

        :quantity: Name of the output field
        :job_id: Unique identifier for the job
        """

        val = None
        
        if self.mpi is False:
            val = self._digQuantityForProcessor(quantity, job_id, 0, copy)
        else:
            val_prank = {}
            nb_rows = np.zeros(self.processors)
            for prank in range(self.processors):
                val_prank[prank] = self._digQuantityForProcessor(quantity, job_id, prank, copy)
                nb_rows[prank] = val_prank[prank].shape[0]

            min_nb_rows = int(np.min(nb_rows))
            val = val_prank[0][:min_nb_rows]
            for i in range(1, self.processors):
                val = np.concatenate((val, val_prank[i][:min_nb_rows]), axis=val.ndim-1)
        return val

    def _digQuantityForProcessor(self, quantity : str, job_id : str, rank : int,  copy : bool):
        val = None
        if copy is True:
            if self.mpi is False:
                database_file = self.path + '/' + self.schema + '/' + job_id + '/tmp.h5'
            elif self.mpi is True:
                database_file = self.path + '/' + self.schema + '/' + job_id + '/tmp_rank-' + str(rank) + '.h5'


        elif copy is False:
            if self.mpi is False:
                database_file = self.path + '/' + self.schema + '/' + job_id + '/database.h5'
            elif self.mpi is True:
                database_file = self.path + '/' + self.schema + '/' + job_id + '/database_rank-' + str(rank) + '.h5'

        f =  h5py.File(database_file, 'r', libver='v110', swmr=True)
        if len(self.steps_to_retrive) == 0:
            dataset = f[quantity]
        else:
            dataset = f[quantity][self.steps_to_retrive, ]
        val  = list(dataset)
        f.close()


        val_as_array = np.asarray(val)
              
        return val_as_array

    def digParameterValue(self, parameter : str, job_id : str):
        job_space =  self.path + '/' + self.schema + '/' + job_id + '.h5'
        with h5py.File(job_space, 'r') as f:
            parameter_value = f['/parameters'].attrs[parameter]
        return parameter_value

    def plotParameterSpace(self, job_name, layout=(4, 4), figsize=(15, 10)):
        dataframe = self.showParameterSpace(job_name, shell_display=False)
        dataframe = dataframe * 1 # to convert boolean string value to 1 or 0
        dataframe.plot(subplots=True, layout=layout, figsize=figsize)

    def showParameterSpaceForJob(self, job_name, shell_display=False):
        param = {}
        job_space =  self.path + '/' + self.schema + '/' + job_name + '.h5'
        with h5py.File(job_space, 'r') as f:
            for key, item in f['/parameters'].attrs.items():
                if key in param.keys():
                    param[key].append(item)
                else:
                    param[key] = []
                    param[key].append(item)

            for key, item in f['/attributes'].attrs.items():
                if key in param.keys():
                    param[key].append(item)
                else:
                    param[key] = []
                    param[key].append(item)

        df = pd.DataFrame(param)
        df = df.sort_values('Time Stamp')

        if shell_display:
            print(f"{bcolors.WARNING}{bcolors.BOLD}Parameters for Job | {job_name}{bcolors.ENDC}")
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df))

        if not shell_display:
            return df

    def showParameterSpaceForParams(self, parameters : dict, shell_display=False):
        job_ids_found = self.digID(parameters)
        if len(job_ids_found) > 1:
            print('Job ID | ', job_ids_found)
            raise RuntimeError('Many jobs exists, choose one')

        job_id = job_ids_found[0]
        self.showParameterSpace(job_id, shell_display)

    def showAdditionalSpaceForParams(self, parameters : dict, shell_display=False):
        job_ids_found = self.digID(parameters)
        if len(job_ids_found) > 1:
            print('Job ID | ', job_ids_found)
            raise RuntimeError('Many jobs exists, choose one')

        job_id = job_ids_found[0]
        self.showAdditionalSpace(job_id, shell_display)

    def showAdditionalSpaceForJob(self, job_id : str, shell_display=False):
        schema_folder = self.path + '/' + self.schema
        parameter_file = schema_folder + '/' + job_id  + '.h5'

        param = {}

        with h5py.File(parameter_file, 'r') as f:
            print(f"{bcolors.WARNING}{bcolors.BOLD}Additional Data for Job | {job_id}{bcolors.ENDC}")
            grp = f.require_group('additionals')

            for key in grp.attrs.keys():
                param[key] = []
                param[key].append(grp.attrs[key])
                if key in grp.keys():
                    dset = grp[key]
                    param[key].append(dset.shape)
                
        df = pd.DataFrame(param)
        if shell_display:
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df))

        if not shell_display:
            return df

    def showDatabaseSpaceForJob(self, job_id : str, shell_display=False):
        job_space =  self.path + '/' + self.schema + '/' + job_id + '.h5'
    
        with h5py.File(job_space, 'r') as f:
            self.processors = f['/attributes'].attrs['Processors']
            if self.processors > 1:
                self.mpi = True
        
        if self.mpi is False:
            database_file = self.path + '/' + self.schema + '/' + job_id + '/database.h5'
        else:
            database_file = self.path + '/' + self.schema + '/' + job_id + '/database_rank-0.h5'

        temporary_file = self.path + '/' + self.schema + '/' + job_id + '/tmp.h5'

        param = {}
        shutil.copy2(database_file, temporary_file)

        with h5py.File(temporary_file, 'r') as f:
            print(f"{bcolors.WARNING}{bcolors.BOLD}Data Quantities in for Job | {job_id} {bcolors.ENDC}")
            for var in f.keys():
                param[var] = []
                param[var].append(f[var].dtype)
                param[var].append(f[var].shape)

        os.remove(temporary_file)

        df = pd.DataFrame(param)
        if shell_display:
            pdtabulate=lambda df:tabulate(df,headers='keys',tablefmt='psql')
            print(pdtabulate(df))

        if not shell_display:
            return df

    def showDatabaseSpaceForParams(self, parameters : dict, shell_display=False):
        job_ids_found = self.digID(parameters)
        if len(job_ids_found) > 1:
            print('Job ID | ', job_ids_found)
            raise RuntimeError('Many jobs exists, choose one')

        job_id = job_ids_found[0]
        self.showDatabaseSpace(job_id, shell_display)

    def deleteJob(self, job_id : str):
        job_space =  self.path + '/' + self.schema + '/' + job_id + '.h5'
        database_folder = self.path + '/' + self.schema + '/' + job_id

        # If file exists, delete it.
        if os.path.isfile(job_space):
            os.remove(job_space)
            
        shutil.rmtree(database_folder, ignore_errors=False, onerror=None)

    def deleteParametricPoint(self, parameters : dict):
        job_ids_found = self.digID(parameters)
        for job_id in job_ids_found:
            self.deleteJob(job_id)
