User Guide
=================

Creating a database
"""""""""""""""""""""

To create a database,  use the class ``Manager``. A manager class takes a path name, schema name and unique identifier for a job:  ``dbmanager.Manager.__init__()`` function:

.. autofunction:: dbmanager.Manager.__init__

To attach a parametric space to a job, initialize the manager class with parameters
``dbmanager.Manager.initialize()`` function:

.. autofunction:: dbmanager.Manager.initialize

- To register an output field, ``Manager`` class accepts a numpy
  array. One can describe as many output fields as one requires. In
  current version, an output field can either be scalar or 1D
  arrays. Make use of function ``dbmanager.Manager.registerQuantities()`` to
  register outfields.

  .. autofunction:: dbmanager.Manager.registerQuantities

- To push the output field values to the database, use the function
  ```dbmanager.Manager.pushQuantity()``

  .. autofunction:: dbmanager.Manager.pushQuantity
		    
		  
To finalize a run use the ``dbmanager.Manager.finalize()`` function:

.. autofunction:: dbmanager.Manager.finalize


The following is the an example script to create a schema.
     
.. literalinclude:: ../../../tests/test_manager.py
   :language: python


Retrieving data
"""""""""""""""""
		  
To retrieve data, use the class ``dbmanager.Digger``.

.. autofunction:: dbmanager.Digger.__init__


- To list all the parametric points (or total jobs) within a schema,
  use the function ``dbmanager.Digger.showParametericSpace()``

To chose a specific a point (or job) from the parameteric space, we
provide a  dedicated class ``Job``.

- To choose a parameteric point, make use of the function
  ``dbmanager.Job.digID()``

  .. autofunction:: dbmanager.Job.digID

  Furthermore, to show the output fields present for the job, on can
  use ``dbmanager.Job.showDatabaseSpace()`` function

  .. autofunction:: dbmanager.Job.showDatabaseSpace

  To retreive, the data for a specific job, two function are
  provided. ``dbmanager.Job.digQuantitiesForJob()`` allows the user to
  retreive data for a specific output field only.
  
  .. autofunction:: dbmanager.Job.digQuantitiesForJob



The following is the an example script to retrieve data from a schema.
     
.. literalinclude:: ../../../tests/test_digger.py
   :language: python
