.. Database-Manager documentation master file, created by
   sphinx-quickstart on Sun Dec 25 11:29:20 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Database-Manager's documentation!
============================================
**Database-Manager** is a Python library for managing data generation and its storage, especially for parameteric studies. **Database-Manager** uses HDF5 files for efficient storage and retrival  of data. 

.. note::
   This project is under active development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./manual/getting_started.rst
   ./manual/user_guide.rst
   ./manual/developer_guide.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
