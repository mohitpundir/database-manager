from dbmanager import Digger, bcolors
import matplotlib.pyplot as plt

path = '.'
schema = 'trial'


digger = Digger(path,schema)

# check the parameteric space of the schema
digger.showParametericSpace(shell_display=True)
digger.plotParametericSpace()

# Point in the parametric space
param_dict = { 'model' : 'old',
               'dt' : 1e-1,
               'time' : 1.0,
               'nodes' : 5,
               'flux' : 1e-3,
               'porosity': 0.4}

# check the database space for a particular point in parameteric space
digger.job.showDatabaseSpace(param_dict, shell_display=True)

# check the additional space for a particular point in parameteric space
digger.job.showAdditionalSpace(param_dict, shell_display=True)

digger.job.showParameterSpace(param_dict, shell_display=True)

print('Job id', digger.job.digID(param_dict)[0])

# extract output for a particular point in parameteric space
val = digger.job.digQuantitiesForJob(['y'], digger.job.digID(param_dict)[0])


print(f"{bcolors.PARAMETERSPACE}Value of y ={bcolors.ENDC}", val['y'])
