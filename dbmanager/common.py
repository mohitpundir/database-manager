import numpy as np
import argparse
import h5py
import datetime
import pandas as pd
import itertools
import glob, os
#from tabulate import tabulate
import subprocess
import shutil
#from multimethod import multimeta
#import matplotlib.pyplot as plt
from mpi4py import MPI


class bcolors:
    RUNID = '\033[95m'
    OKBLUE = '\033[94m'
    PARAMETERSPACE = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
